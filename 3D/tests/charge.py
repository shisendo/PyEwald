#!/usr/bin/python3

"""
.. module:: NaCl.py
    :synopsis: Obtain the Madelung constant for NaCl using the charge-charge interaction matrix.

.. moduleauthor:: D. Wang <dwang5@zoho.com>


See the notes for 'CsCl'.
For NaCl, the value is  :math:`M_0=-0.8738`.

"""

import sys
import os
sys.path.append("../src/")

import os.path
from netCDF4 import Dataset
import numpy as np
from charge_charge import Charge_charge
from configuration2D import *
from utility import *
p = sys.argv
if len(p)!=4:
    print("inputs error! ")
    exit()
n1 = int(p[1])
n2 = int(p[2])
n3 = int(p[3])

# ----- NaCl -----
lattice = np.array([[1,0,0],[0,1,0],[0,0,1]])
nc_file = "ccmat-{n1}x{n2}x{n3}.nc".format(n1=n1,n2=n2,n3=n3)
calculated = os.path.exists(nc_file)
if (not calculated):
    cc = Charge_charge(n1,n2,n3,lattice)
    cc.write_charge_matrix(nc_file)

#Now read the charge-charge matrix.
dataset = Dataset(nc_file,'r')
cc_mat = dataset['matrix']

g_alloy = Charge_config(n1,n2,n3,lattice)
g_alloy.generate_high_symmetry(p=1,k=(1,1,1))
alloy_fn = "NaCl_config.nc"
g_alloy.write_alloy_matrix(alloy_fn)
rslt = calc_madelung_cc(cc_mat,alloy_fn)

print("Madelung constant (NaCl): ", rslt)
os.system("rm NaCl_config.nc")
