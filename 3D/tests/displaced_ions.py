#!/usr/bin/python3

"""
.. module:: displaced_ions.py
    :synopsis: Obtain the energy due to the displacement of ions.

.. moduleauthor:: D. Wang <dwang5@zoho.com>

In this test, we set up a uniformly displaced charegs in a cubic lattice.
An application of this type of interaction matrix will be shown in a
later publications.

"""

import sys
import os
sys.path.append("../src/")

import os.path
from netCDF4 import Dataset
import numpy as np
from dipole_dipole import Dip_dip

from configuration2D import *
from charge_displacement import Charge_displacement
from utility import *
from math import *

n1 = 2
n2 = 2
nz = 2
#
## First get the dipole matrix ready.
lattice = np.array([[1,0,0],[0,1,0],[0,0,1]])
nc_file = "charge_shift-matrix_{n1}x{n2}x{nz}.nc".format(n1=n1,n2=n2,nz=nz)
calculated = os.path.exists(nc_file)
if (not calculated):
    dp = Charge_displacement(n1,n2,nz,lattice)
    dp.write_dipole_matrix(nc_file)

##Now read the charge-charge matrix.
dataset = Dataset(nc_file,'r')
dd_mat = dataset['matrix']
#
##----Gamma point ----
g_alloy = Dipole_config(n1,n2,nz,lattice)
g_alloy.generate_high_symmetry(p=(0,0,1),k_px=(0,0,0),k_py=(0,0,0),k_pz=(0,0,0))
alloy_fn = "dd_Gamma.nc"
g_alloy.write_alloy_matrix(alloy_fn)
rslt = calc_madelung_dd(dd_mat,alloy_fn)
print("Gamma Point: ", rslt)
