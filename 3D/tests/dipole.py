#!/usr/bin/python3

"""
.. module:: dipole.py
    :synopsis: Obtain the energy due to the diploe-dipole interaction for various dipole configurations.

.. moduleauthor:: D. Wang <dwang5@zoho.com>


The configuration can be found in Zhong et al (PRB, 52, 6301).
Results for comparison is given by Nishimatsu et al in units of Z*^2/epsilon*a0^3
(Phys. Rev. B,82,134106 (2010).

+----------+----------+
| k        |    value |
+==========+==========+
| Gamma    |  -2/3*pi |
+----------+----------+
| X1       |  4.84372 |
+----------+----------+
| X5       | -2.42186 |
+----------+----------+
| M3       | -2.67679 |
+----------+----------+
| M5       |  1.33839 |
+----------+----------+
| R25      |        0 |
+----------+----------+
| Simga_Lo |  2.93226 |
+----------+----------+


"""

import sys
import os
sys.path.append("../src/")

import os.path
from netCDF4 import Dataset
import numpy as np
from charge_charge import Charge_charge
from dipole_dipole import Dip_dip
from configuration import *
from utility import *
from math import *

p = sys.argv
if len(p)!=4:
    print("inputs error! ")
    exit()
n1 = int(p[1])
n2 = int(p[2])
n3 = int(p[3])

#
## First get the dipole matrix ready.
lattice = np.array([[1, 0, 0], [0,1,0], [0,0,1]])
nc_file = "dd-matrix_{n1}x{n2}x{n3}.nc".format(n1=n1,n2=n2,n3=n3)
calculated = os.path.exists(nc_file)
if (not calculated):
    dp = Dip_dip(n1,n2,n3, lattice)
    dp.write_dipole_matrix(nc_file)
#
##Now read the charge-charge matrix.
dataset = Dataset(nc_file,'r')
dd_mat = dataset['matrix']
#
##----Gamma point ----
g_alloy = Dipole_config(n1,n2,n3,lattice)
g_alloy.generate_high_symmetry(p=(0, 0, 1), k_px=(0, 0, 0),k_py=(0, 0, 0),k_pz=(0, 0, 0))
alloy_fn = "dd_Gamma.nc"
g_alloy.write_alloy_matrix(alloy_fn)
rslt = calc_madelung_dd(dd_mat,alloy_fn)
print("Gamma Point: ", rslt)
##----X1 point ----
X1_alloy = Dipole_config(n1,n2,n3,lattice)
X1_alloy.generate_high_symmetry(p=(0,0,1),k_px=(0,0,0),k_py=(0,0,0),k_pz=(0,0,1))
alloy_fn = "dd_X1.nc"
X1_alloy.write_alloy_matrix(alloy_fn)
#
rslt = calc_madelung_dd(dd_mat,alloy_fn)
print("X1 Point: ", rslt)
##----X5 point ----
X1_alloy = Dipole_config(n1,n2,n3,lattice)
X1_alloy.generate_high_symmetry(p=(1,0,0),k_px=(0,0,1),k_py=(0,0,0),k_pz=(0,0,0))
alloy_fn = "dd_X5.nc"
X1_alloy.write_alloy_matrix(alloy_fn)
#
rslt = calc_madelung_dd(dd_mat,alloy_fn)
print("X5 Point: ", rslt)
##----M3 point ----
X1_alloy = Dipole_config(n1,n2,n3,lattice)
X1_alloy.generate_high_symmetry(p=(0,0,1),k_px=(0,0,0),k_py=(0,0,0),k_pz=(1,1,0))
alloy_fn = "dd_M3.nc"
X1_alloy.write_alloy_matrix(alloy_fn)
#
rslt = calc_madelung_dd(dd_mat,alloy_fn)
print("M3 Point: ", rslt)
##----M5 point ----
X1_alloy = Dipole_config(n1,n2,n3,lattice)
X1_alloy.generate_high_symmetry(p=(1,0,0),k_px=(1,1,0),k_py=(0,0,0),k_pz=(0,0,0))
alloy_fn = "dd_M5.nc"
X1_alloy.write_alloy_matrix(alloy_fn)

rslt = calc_madelung_dd(dd_mat,alloy_fn)
print("M5 Point: ", rslt)
##----R25 point ----
X1_alloy = Dipole_config(n1,n2,n3,lattice)
X1_alloy.generate_high_symmetry(p=(0,0,1),k_px=(0,0,0),k_py=(0,0,0),k_pz=(1,1,1))
alloy_fn = "dd_R25.nc"
X1_alloy.write_alloy_matrix(alloy_fn)
#
rslt = calc_madelung_dd(dd_mat,alloy_fn)
print("R25 Point: ", rslt)

##----Simga_LO point ----
## This one truly needs a 4x4x4 supercell.
X1_alloy = Dipole_config(n1,n2,n3,lattice)
X1_alloy.generate_high_symmetry(p=(1/sqrt(2.0),0,1/sqrt(2.0)),
        k_px=(0.5,0,0.5),k_py=(0.0,0,0.0),k_pz=(0.5,0,0.5))
alloy_fn = "dd_LO.nc"
X1_alloy.write_alloy_matrix(alloy_fn)
#
rslt = calc_madelung_dd(dd_mat,alloy_fn)
print("Sigma_LO Point: ", rslt)
os.system("rm dd_*")
