#!/usr/bin/python3

"""
.. module:: dipole.py
    :synopsis: Obtain the energy due to the diploe-dipole interaction for the 2D dipoles.  Here
        we pretend this is a 2D structure by use longer distance in the z direction.

.. moduleauthor:: D. Wang <dwang5@zoho.com>

"""

import sys
import os

sys.path.append("../src/")

import os.path
from netCDF4 import Dataset
import numpy as np
from charge_charge import Charge_charge
from dipole_dipole import Dip_dip
from configuration import *
from utility import *
from math import *

n1 = 2
n2 = 2
n3 = 8

#
## First get the dipole matrix ready.
lattice = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
nc_file = "dd-matrix_{n1}x{n2}x{n3}.nc".format(n1=n1, n2=n2, n3=n3)
calculated = os.path.exists(nc_file)
if (not calculated):
    dp = Dip_dip(n1, n2, n3, lattice)
    dp.write_dipole_matrix(nc_file)
#
##Now read the charge-charge matrix.
dataset = Dataset(nc_file, 'r')
dd_mat = dataset['matrix']
#
##----Gamma point ----
g_alloy = Dipole_config(n1, n2, n3, lattice)
g_alloy.generate_high_symmetry(p=(1, 0, 0), k_px=(1, 1, 1), k_py=(0, 0, 0), k_pz=(0, 0, 0))

# Set all the dipoles to be zero except the bottom layer.
for i in range(n1):
    for j in range(n2):
        for k in range(1, n3):
            g_alloy.set_dipole(i, j, k, [0, 0, 0])
alloy_fn = "dd_Gamma.nc"
g_alloy.write_alloy_matrix(alloy_fn)

rslt = calc_madelung_dd(dd_mat, alloy_fn)
# print("Gamma Point: ", rslt*32.0/4.0)
print("Gamma Point: ", rslt)
