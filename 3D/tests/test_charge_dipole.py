#!/usr/bin/python3
"""
.. module:: test_charge_dipole.py
    :synopsis: Test the charge-dipole interaction matrix with a few charge and dipole configurations.

.. moduleauthor:: D. Wang <dwang5@zoho.com>


One particular configuration is that

1. The sign of charge alternates along z.
2. Each dipole only has the p_z component, which also alternates along z.

The charge-dipole energy is easy to estimate. In the folder
"tests/verify_charge-dipole", we also have a program to directly calculate this
value in real space. The result agrees with what we have obtained here using
the charge-dipole interaction matrix.

"""

import sys
import os

sys.path.append("../src/")

import os.path
from netCDF4 import Dataset
import numpy as np
from charge_dipole import Charge_dipole
from configuration2D import Charge_config, Dipole_config
from utility import calc_madelung_cd


n1 = 2
n2 = 2
nz = 2

## First get the dipole matrix ready.
lattice = np.array([[1,0,0],[0,1,0],[0,0,1]])
nc_file = "cd-matrix_{n1}x{n2}x{nz}.nc".format(n1=n1,n2=n2,nz=nz)
calculated = os.path.exists(nc_file)
if (not calculated):
    dp = Charge_dipole(n1,n2,nz,lattice)
    dp.write_cd_matrix(nc_file)
#
##Now read the charge-charge matrix.
dataset = Dataset(nc_file,'r')
cd_mat = dataset['matrix']
#

# Dipole at X point; charge at X point
g_alloy = Dipole_config(n1,n2,nz,lattice)
g_alloy.generate_high_symmetry(p=(0,0,1.0), k_px=(0,0,0),k_py=(0,0,0),k_pz=(0,0,1))
g_fn = "cd_dipole.nc"
g_alloy.write_alloy_matrix(g_fn)

h_alloy = Charge_config(n1,n2,nz,lattice)
h_alloy.generate_high_symmetry(p=1.0,k=(0,0,1))
h_fn = "cd_charge.nc"
h_alloy.write_alloy_matrix(h_fn)

rslt = calc_madelung_cd(cd_mat,h_fn,g_fn,g_alloy)
print("X + X: ", rslt)

# uniform charge, uniform dipole.
h_alloy = Charge_config(n1,n2,nz,lattice)
h_alloy.generate_high_symmetry(p=1.0,k=(0,0,0))
h_fn = "cd_charge_2.nc"
h_alloy.write_alloy_matrix(h_fn)
rslt = calc_madelung_cd(cd_mat,h_fn,g_fn,g_alloy)
print("Gamma + Gamma: ", rslt)

# x-y checkboard charge, uniform dipole.
h_alloy = Charge_config(n1,n2,nz,lattice)
h_alloy.generate_high_symmetry(p=1.0,k=(1,1,0))
h_fn = "cd_charge_xy-checkboard.nc"
h_alloy.write_alloy_matrix(h_fn)
rslt = calc_madelung_cd(cd_mat,h_fn,g_fn,g_alloy)
print("xy checkboard + Gamma: ", rslt)

# x-y checkboard charge, x-y checkboard dipole.
g_alloy = Dipole_config(n1,n2,nz,lattice)
g_alloy.generate_high_symmetry(p=(0, 0, 1.0), k_px=(0, 0, 0), k_py=(0, 0, 0), k_pz=(1, 1, 0))
g_fn = "cd_dipole_xy-checkboard.nc"
g_alloy.write_alloy_matrix(g_fn)

print("xy checkboard + xy checkboard ", rslt)
