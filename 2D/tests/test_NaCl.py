#!/usr/bin/python3

"""
.. module:: NaCl.py
    :synopsis: Obtain the Madelung constant for 2D 2D NaCl using the charge-charge interaction matrix.

.. moduleauthor:: D. Wang <dwang5@zoho.com>


See the notes for 'CsCl'.
For NaCl, the value is  :math:`M_0=-1.613843/2 = 0.806922`.

"""

import sys
import os
sys.path.append("../src/")

import os.path
from netCDF4 import Dataset
import numpy as np
from charge_charge2D import Charge_charge2D
#from configuration import *
#from utility import *
p = sys.argv
#if len(p)!=4:
#    print("inputs error! ")
#    exit()
#n1 = int(p[1])
#n2 = int(p[2])
#n3 = int(p[3])

n1 = 4
n2 = 4
n3 = 1

# ----- NaCl -----
lattice = np.array([[1,0,0],[0,1,0],[0,0,1]])
nc_file = "ccmat2D-{n1}x{n2}x{n3}.nc".format(n1=n1,n2=n2,n3=n3)
calculated = os.path.exists(nc_file)
if (not calculated):
    cc = Charge_charge2D(n1,n2,n3,lattice)
    cc.write_charge_matrix(nc_file)

#Now read the charge-charge matrix.
dataset = Dataset(nc_file,'r')
cc_mat = dataset['matrix']
print(cc_mat[:])
charge = [1,-1,1,-1,
          -1,1,-1,1,
          1,-1,1,-1,
          -1,1,-1,1]
mdl = 0.0
for i in range(16):
    mdl += charge[i]*cc_mat[i]

print(mdl*2)
print("real value:",-1.613843)
