import sys
sys.path.append("../src/")
import numpy as np
from supercell import Supercell
import matplotlib.pyplot as plt
import math
import os
from netCDF4 import Dataset
from charge_charge2D import Charge_charge2D
#p = sys.argv
#if len(p)!=4:
#    print("inputs error! ")
#    exit()
n1 = 8
n2 = 8
n3 = 20

# ----- NaCl -----
lattice = np.array([[1,0,0],[0,1,0],[0,0,1]])
nc_file = "ccmat2D-{n1}x{n2}x{n3}.nc".format(n1=n1,n2=n2,n3=n3)
calculated = os.path.exists(nc_file)
if (1):
    cc = Charge_charge2D(n1,n2,n3,lattice)
    cc.write_charge_matrix(nc_file)

#Now read the charge-charge matrix.
dataset = Dataset(nc_file,'r')
cc_mat = dataset['matrix']

p1 = [0,0,0]
p2 = [0,0,1]

q1 = +1
q2 = -1
a0 = 8.50377
e = np.zeros(10)
f = np.zeros(9)
for i in range(11)[1:]:
    e[i-1] = 2.0*q1*q2*cc_mat[cc.iaa[0,0,i]]/a0
    print(cc_mat[cc.iaa[0,0,i]])
    if i>1:
        f[i-2] = e[i-1]-e[i-2]

plt.figure()
plt.plot(e,'r',label="energy")
plt.plot(f,'g',label="force")
plt.legend()
plt.show()
