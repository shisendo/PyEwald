"""
.. module:: dipole.py
    :synopsis: Obtain the energy due to the diploe-dipole interaction for various dipole configurations in 2D setting..

.. moduleauthor:: D. Wang <dwang5@zoho.com>

The configuration can be found in Zhong et al (PRB, 52, 6301).
Results for comparison is given by Nishimatsu et al in units of Z*^2/epsilon*a0^3
(Phys. Rev. B,82,134106 (2010).
"""

import sys
import os

sys.path.append("../src/")

import os.path
from netCDF4 import Dataset
import numpy as np
from dipole_dipole2D import Dip_dip2D
from configuration2D import Dipole_config


def calc_madelung_dd(mat, alloy_fn):
    """
    Args:
       * 'mat' is the dipole-dipole interaction matrix.
       * 'alloy_fn' specifies the dipole configuration.

    Calculate the equivalent "Madelung constant" due to the dipole-dipole interaction.
    """
    dum_1 = Dataset(alloy_fn, 'r')
    config = dum_1['matrix']

    delung = 0.0
    v1 = np.array([config[0, 0], config[0, 1], config[0, 2]])

    for i in range(config.shape[0]):
        dum = np.array([
            [mat[i, 0], mat[i, 5], mat[i, 4]],
            [mat[i, 5], mat[i, 1], mat[i, 3]],
            [mat[i, 4], mat[i, 3], mat[i, 2]]
        ])
        v2 = np.array([config[i, 0], config[i, 1], config[i, 2]])
        delung += np.dot(v1, np.dot(dum, v2))

    return delung


n1 = 2
n2 = 2
n3 = 1

## First get the dipole matrix ready.
lattice = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
nc_file = "ddmat2D-{n1}x{n2}x{n3}.nc".format(n1=n1, n2=n2, n3=n3)
calculated = os.path.exists(nc_file)
# if (not calculated):
if 1:
    dp = Dip_dip2D(n1, n2, n3, lattice)
    dp.write_dipole_matrix(nc_file)

## Get the dipole-dpole interaction matrix.
dataset = Dataset(nc_file, 'r')
dd_mat = dataset['matrix']
# print(dd_mat[:])

# Generate the configuration.
## -+
## +-
alloy = Dipole_config(n1, n2, n3, lattice)
alloy.generate_high_symmetry(p=(1, 0, 0), k_px=(1, 1, 0), k_py=(0, 0, 0), k_pz=(0, 0, 0))
alloy_fn = "dd_alternating.nc"
alloy.write_alloy_matrix(alloy_fn)
rslt = calc_madelung_dd(dd_mat, alloy_fn)
print("Case 1: 2D calculation: {} ".format(rslt))

## --
## ++
alloy = Dipole_config(n1, n2, n3, lattice)
alloy.generate_high_symmetry(p=(1, 0, 0), k_px=(0, 1, 0), k_py=(0, 0, 0), k_pz=(0, 0, 0))
alloy_fn = "dd_alternating_along_y.nc"
alloy.write_alloy_matrix(alloy_fn)
rslt = calc_madelung_dd(dd_mat, alloy_fn)
print("Case 2: 2D calculation: {} ".format(rslt))

## -+
## -+
alloy = Dipole_config(n1, n2, n3, lattice)
alloy.generate_high_symmetry(p=(1, 0, 0), k_px=(1, 0, 0), k_py=(0, 0, 0), k_pz=(0, 0, 0))
alloy_fn = "dd_alternating_along_x.nc"
alloy.write_alloy_matrix(alloy_fn)
rslt = calc_madelung_dd(dd_mat, alloy_fn)
print("Case 3: 2D calculation: {} ".format(rslt))
